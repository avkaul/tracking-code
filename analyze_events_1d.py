import ROOT
import numpy
from ROOT import TCanvas, TH2F
ROOT.gROOT.SetBatch(True)
#import matplotlib.pyplot as plt

def main():
	trueevents = 0
	maxevents = 10
	num = 500
	events = numpy.zeros((20, 20))

	filen = f'ForEFTracking/1kevents/hits.txt'
	with open(filen, 'r') as file:
		lines = file.readlines()
		lines.pop(0)

		arr = []

		int0 = int(lines[0][0])
		for line in lines:
			seg = line.split(',')
			if int(seg[0]) == int0:
			 	arr.append([int(seg[0]), float(seg[3]), float(seg[4])])
			elif trueevents == 10:
				break
			else:
				print(trueevents)
				events += numpy.array(test_hits_2d(arr)) #test_hits(arr, int(seg[0]), num))
				trueevents += 1
				arr = []
				int0 = int(seg[0])
				arr.append([int(seg[0]), float(seg[3]), float(seg[4])])
	
	# bins = []
	# for b in range(1, 100, 10):
	# 	bins.append(b)

	y = events/trueevents
	print(y)
	numpy.savetxt("2d_out/1.txt", y)
	

	# plt.plot(bins, events/trueevents, '-o')
	# plt.xlabel("No. of bins")
	# plt.ylabel("Track Finding Efficiency (10events)")
	# #plt.show()
	# plt.savefig("nov_last/test.png")

def test_hits(arr, event, num):
	maxbins = 5000
	result = []

	file_length = len(arr)

	for xbins in range(1, num, 10):
		x = TCanvas()
		h2 = TH2F("h1", "title", 150, 0, 1.5, bins, -1000, 1000)
		
		for i in range(0, file_length):
			for phi in range(0, 360):
				phi0 = phi*1.0*numpy.pi/180
				d0 = (float(arr[i][2]) - float(arr[i][1])*numpy.tan(phi0))/numpy.cos(phi0)
				h2.Fill(phi0, d0, 1)

		h2.Draw()
		x.Print(f"2d_out/{event}-{bins}.png")

		hit = 0;
		for i in range(1, bins + 1):
			for j in range(1, bins + 1):
				gbin = h2.GetBin(i, j);
				w = h2.GetBinContent(gbin);
				if (w > 7):
					hit = 1
					break
		result.append(hit)
		del h2;
	
	return result

def test_hits_2d(arr):
	maxbins = 5000
	result = []

	file_length = len(arr)

	for xbins in range(1, 500, 5):
		yres = []
		print(f'	{xbins}')
		for ybins in range(1, 500, 5):
			print(f'		{ybins}')
			h2 = TH2F("h1", "title", xbins, 0, 1.5, ybins, -1000, 1000)
			
			for i in range(0, file_length):
				for phi in range(0, 360):
					phi0 = phi*1.0*numpy.pi/180
					d0 = (float(arr[i][2]) - float(arr[i][1])*numpy.tan(phi0))/numpy.cos(phi0)
					h2.Fill(phi0, d0, 1)

			hit = 0;
			for i in range(1, xbins + 1):
				for j in range(1, ybins + 1):
					gbin = h2.GetBin(i, j);
					w = h2.GetBinContent(gbin);
					if (w > 7):
						hit = 1
						break

			yres.append(hit)
			del h2;
			
		result.append(yres)
		
	return result

if __name__ == "__main__":
	main()