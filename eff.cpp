#include <cmath>
#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <sstream>
#include <chrono>

#include "TH2.h"
#include "TCanvas.h"

using namespace std;
using namespace std::chrono;

int main(){
	int max_events = 1000;
	string target = "ForEFTracking/1kevents/events/";
	
	int file_length = 10; //8885, 9
	int maxbin = 10000;
	for (int k = 0; k < max_events; k++){
		string x = target.append(to_string(k));
		ifstream input(x.append(".txt"));

		string arr[file_length][6];
		string line, word;

		int i = 0;
		while (getline(input, line)){
			istringstream iss(line);
			int j = 0;
			while (getline(iss, word, ',')){
				arr[i][j] = word;
				j = j + 1;
			}
			i = i + 1;
		}

		for (int bins = 100; bins < maxbin; bins = bins + 100){
			TH2* h2 = new TH2F("h1", "title", bins, 0, 1.5, bins, -1000, 1000) ; // weird bounds?
			
			for (int i = 1; i < file_length; i++){
				float d0;
				for (int phi = 0; phi < 360; phi++){
					float phi0 = phi*1.0*M_PI/180;
					cout << arr[i][4] << "," << arr[i][3];
					// d0 = (stof(arr[i][4]) - stof(arr[i][3])*tan(phi0))/cos(phi0);
					h2->Fill(phi0, d0, 1);
					}
				}

			int num = 0;
			for (int i = 1; i < bins + 1; i++){
				for (int j = 1; j < bins + 1; j++){
					int gbin = h2->GetBin(i, j);
					int w = h2->GetBinContent(gbin);
					if (w > 8){
						num = num + 1;
					}
				}
			}
			cout << num << ",";// << num << endl;
			delete h2;
			}
	return 0;
	}
}

		// auto start = high_resolution_clock::now();
// auto stop = high_resolution_clock::now();
	// auto duration = duration_cast<microseconds>(stop - start);
	 
	//cout << "Time taken by function: " << " ";
	// cout << duration.count() << ","; //<< " microseconds" << endl;

	//h2->Draw();
	//c->Print((to_string(bins) + ".png").c_str());
//	}


