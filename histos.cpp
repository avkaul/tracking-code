// math imports
#include <cmath>
#include <cstdlib>
#include <vector>

// file-handling imports
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>

//ROOT imports
#include "TH2.h"
#include "TCanvas.h"

using namespace std;

using tMatrix = std::vector<std::vector<double>>;

void dumpVector(vector<vector<double>> vec){
  cout << "Vector \n";
  for (const auto &item: vec){
    for (const auto &num: item){
      cout << num << ",";
    }
  cout << "\n";
  }
}

void dumpMatrix(const tMatrix matrix) {
    std::cout << "Matrix \n";
    for (const auto &row: matrix) {
        for (const auto &item: row) {
            std::cout << item << ",";
        }
        std::cout << '\n';
    }
}

void plotResults(vector<vector<double>> &file_contents, int xparam, int yparam){
  TCanvas *c1=new TCanvas();
  TH2* h2 = new TH2F("h1", "title", xparam, 0, 1.5, yparam, -1000, 1000); // initialize TH2F
  // fill TH2F
  // cout << xparam << "," << yparam;
  // dumpVector(file_contents);
  for (unsigned long i = 0; i < file_contents.size(); i++){
    double d0;
    for (int phi = 0; phi < 360; phi++){ // for all possible phi in degrees, 
      
      
      double phi0 = phi*1.0*M_PI/180; // find phi0 in radians
      d0 = (file_contents[i][0] - file_contents[i][1]*tan(phi0))/cos(phi0); // Hough Transform on phi0
      

      // double x = tan(phi0)/cos(phi0);
      // cout << phi << "," << phi0 << "," << d0 << "," << x << "\n";
      h2->Fill(phi0, d0, 1); // and pass into TH2F
    }
  } 
  
  // Check for hit
  bool hit = false;
    for (int i=1; i<xparam+1; i++){
      for (int j=1; j<yparam+1; j++){
        int gbin = h2->GetBin(i, j); // step through all bins and find bin number
        int w = h2->GetBinContent(gbin); // get weight of each bin
        if (w > 7){
          hit = true;
        }
      }
    }

  const char* title = (hit ? "True" : "False");
  h2->SetTitle(title);
  h2->GetXaxis()->SetTitle("No. of bins in phi");
  h2->GetXaxis()->SetTitleOffset(1.5);
  h2->GetXaxis()->CenterTitle(true);
  h2->SetYTitle("No. of bins in d0");
  h2->GetYaxis()->CenterTitle(true);
  h2->GetYaxis()->SetTitleOffset(2);
  h2->GetZaxis()->SetTitleOffset(1);
  h2->GetZaxis()->CenterTitle(true);
  h2->SetZTitle("No. of hits");
  h2->Draw();

  string filename = "output/" + to_string(xparam) + "-" + to_string(yparam) + "-cpp.png";

  c1->SetCanvasSize(1000, 800);
  c1->SaveAs(filename.c_str());

  delete h2;
  delete c1;
}

vector<vector<bool>> test_hits_2d(vector<vector<double>> &file_contents, int xparams[3], int yparams[3]){
  // passing data into our root-specific functions, and storing the output as a 2D vector

  vector<vector<bool>> x_results; 
  for (int xbins = xparams[0]; xbins < xparams[1]; xbins += xparams[2]){
    vector<bool> y_results;
    for (int ybins = yparams[0]; ybins < yparams[1]; ybins += yparams[2]){

      TH2* h2 = new TH2F("h1", "title", xbins, 0, 1.5, ybins, -1000, 1000); // initialize TH2F
      // fill TH2F
      for (unsigned long i = 0; i < file_contents.size(); i++){
        double d0;
        for (int phi = 0; phi < 360; phi++){ // for all possible phi in degrees, 
          double phi0 = phi*1.0*M_PI/180; // find phi0 in radians

          // d0 = (yhit - xhit * tan(phi0)) / cos(phi0)
          d0 = (file_contents[i][0] - file_contents[i][1]*tan(phi0))/cos(phi0); // Hough Transform on phi0
          h2->Fill(phi0, d0, 1); // and pass into TH2F
        }
      }
    
      // check TH2F
      bool hit = false;
      for (int i=1; i<xbins+1; i++){
        for (int j=1; j<ybins+1; j++){
          int gbin = h2->GetBin(i, j); // step through all bins and find bin number
          int w = h2->GetBinContent(gbin); // get weight of each bin
          if (w > 7){
            hit = true;
          }
        }
      }
      
      // return results
      y_results.push_back(hit);
      delete h2;
    }
    
    x_results.push_back(y_results);
    y_results.clear();
  }

  // dumpMatrix(x_results);
  return x_results;
}

int main(int argc, char *argv[]){
  // loading CSV file into a vector of strings
  string filename = "ForEFTracking/hits.txt";
  ifstream input(filename);

  vector<vector<string>> file_contents;
  string line, word;

  // arguments in case they are passed
  string event_no;
  int d0bins = 0; int phi0bins = 0;
  
  // load file contents into 2d vector of strings
  while (getline(input, line)){
    istringstream iss(line);
    vector<string> words;
    while (getline(iss, word, ',')){
      words.push_back(word);
    }
    file_contents.push_back(words);
  }
  
  // iterate over and pass only lines w/ same
  // event number into test_hits_2d
  
  string current_event = file_contents[0][0];
  int true_events = 0;
  vector<vector<double>> shared_event;
  
  int xparams[3] = {1, 250, 5}; // min, max, increment
  int yparams[3] = {1, 250, 5}; // min, max, increment
  
  int xrange = (xparams[1] - xparams[0]) / xparams[2] + 1;
  int yrange = (yparams[1] - yparams[0]) / yparams[2] + 1;

  // logic for plotting individual events. accepts command line arguments.
  if (argc > 1){
    event_no = argv[3];
    d0bins = atoi(argv[1]);
    phi0bins = atoi(argv[2]);
  }

  vector<vector<int>> events(xrange, vector<int> (yrange, 0));

  for (unsigned long i = 0; i < file_contents.size(); i++){
    // stored in d0_phi as y, x
    vector<double> d0_phi = {stof(file_contents[i][4]), stof(file_contents[i][3])};
    
    if (file_contents[i][0].compare(current_event) == 0){
      // cout << file_contents[i][0] << "," << current_event << "\n";
      shared_event.push_back(d0_phi);    
    }
    else{
      cout << "   " << current_event << "\n";
      
      if (argc > 1){
        if (current_event == event_no){

        //dumpMatrix(shared_event);
        plotResults(shared_event, d0bins, phi0bins);
        return 0;
        }
      }

      else {
        vector<vector<bool>> single_event_arr = test_hits_2d(shared_event, xparams, yparams);
        for (unsigned long i = 0; i < single_event_arr.size(); i++){
          for (unsigned long j = 0; j < single_event_arr[i].size(); j++){
            if (single_event_arr[i][j]){
              events[i][j] += 1; 
            }
          }
        }
      true_events += 1;
      }

      // define new shared_event vector
      // and start a new loop
      shared_event.clear();
      shared_event.push_back(d0_phi);

      // update current event counter
      current_event = file_contents[i][0];
    }

  }
  
  fstream newfile;
  newfile.open("cppout.txt", ios_base::out);

  for (unsigned long i = 0; i < events.size(); i++){
    for (unsigned long j = 0; j < events[i].size(); j++){
      int out = (int)events[i][j]; 
      newfile << std::setprecision(5) << out << ",";
    }
    newfile << "\n";
  }
}

