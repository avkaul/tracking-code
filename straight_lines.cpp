#include <cmath>
#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <sstream>

#include "TH2.h"
#include "TCanvas.h"

using namespace std;
int to_phi_space(float x, float y, vector<pair<int, vector<float>>> &container){
	float d0;
	float eps = 0.1;

	for (int phi = 0; phi < 360; phi++){
		float phi0 = phi*1.0*M_PI/180;
		d0 = (y - x*tan(phi0))/cos(phi0);

		vector<float> vals = {d0, phi0};
		
		// check if it is in the container. if so, increments counter
		for (int i = 0; i < container.size(); i++){
			if ((abs(container[i].second[0] - d0) < eps) & (abs(container[i].second[1] - phi < eps))){
				container[i] = make_pair(container[i].first + 1, container[i].second);
				return 0;
			}
		}

		// if not in container, add it
		container.push_back(make_pair(1, vals));	
	}
	return 0;
}

int main(){
	vector<pair<int, vector<float>>> container; // pair: no. of hits, vector of d0, phi
	
	auto c = new TCanvas("c", "title");
	TH2* h2 = new TH2F("h1", "title", 1000, 0, 3.15, 1000, -1000, 1000) ; // weird bounds?
	ifstream input("ForEFTracking/test.txt"); // 1kevents/hits.txt");
	
	int file_length = 9;
	
	string arr[file_length][6];
	string line, word;

	int i = 0;
	while (getline(input, line)){
		istringstream iss(line);
		int j = 0;
		while (getline(iss, word, ',')){
			arr[i][j] = word;
			j = j + 1;
		}
		i = i + 1;
	}

	for (int i = 1; i < file_length; i++){
		cout << arr[i][3] << ",";
		cout << arr[i][4] << endl;
	 	to_phi_space(stof(arr[i][3]), stof(arr[i][4]), container);	
	}
	for (int i = 0; i < container.size(); i++){
		cout << container[i].first << " ";
		cout << container[i].second[0] << " ";
		cout << container[i].second[1] << endl;
		h2->Fill(container[i].second[1], container[i].second[0]); // container[i].first); // x is phi, y is d0, weights also added
	}

	h2->Draw();
	c->Print("test.png");

	return 0;
}
