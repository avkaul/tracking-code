import matplotlib.pyplot as plt
import numpy as np
from numpy import math

info = {"histos/output.txt":[[1, 250, 5], [1, 250, 5]],} #"output.txt":[[1, 1000, 100], [1, 500, 100]], 

for i, j in info.items():
    data = np.loadtxt(i,) 
    
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    
    d0 = [i for i in range(*j[0])]
    phi = [i for i in range(*j[1])]
    
    d0, phi = np.meshgrid(phi, d0)
    
    ax.set_xlabel("No. of bins in d0")
    ax.set_ylabel("No. of bins in phi0")
    ax.set_zlabel("Efficiency")
    ax.plot_wireframe(d0, phi, data)
    plt.show()
    plt.savefig(f'{i[:-4]}.jpg')
