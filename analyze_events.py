import ROOT
import numpy
from ROOT import TCanvas, TH2F
from os.path import exists

def main():
	trueevents = 0
	maxevents = 5
	events = numpy.zeros((5, 10))
	for i in range(0, maxevents):
		file = f'ForEFTracking/1kevents/events/{i}.txt'
		if exists(file):
			print(i)
			events += numpy.array(test_hits(file))
			trueevents += 1
	print(events/1000)


def test_hits(file):
	maxbins = 5000
	result = []

	with open(file,"r") as f:
		arr = f.readlines()
		file_length = len(arr)

	for xbins in range(1, 500, 25):
		yres = []
		for ybins in range(1, 500, 25):
			h2 = TH2F("h1", "title", xbins, 0, 1.5, ybins, -1000, 1000)
			
			for i in range(0, file_length):
				for phi in range(0, 360):
					phi0 = phi*1.0*numpy.pi/180
					seg = arr[i].split(',')
					d0 = (float(seg[2]) - float(seg[1].replace("\n", ""))*numpy.tan(phi0))/numpy.cos(phi0)
					h2.Fill(phi0, d0, 1)

			hit = 0;
			for i in range(1, xbins + 1):
				for j in range(1, ybins + 1):
					gbin = h2.GetBin(i, j);
					w = h2.GetBinContent(gbin);
					if (w > 7):
						hit = 1
						break
			yres.append(hit)
			del h2;
			
		result.append(yres)
		
	return result

if __name__ == "__main__":
	main()