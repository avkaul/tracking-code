import ROOT
import numpy
from ROOT import TH2F
import time
import sys
ROOT.gROOT.SetBatch(True)
#import matplotlib.pyplot as plt

def main():
	trueevents = 1 

	events = numpy.zeros((len([i for i in range(140, 150, 1)]),
					len([i for i in range(140, 150, 1)])))

	d0bins, phibins, event = sys.argv[1], sys.argv[2], sys.argv[3]

	filen = 'ForEFTracking/hits.txt'
	with open(filen, 'r') as file:
		lines = file.readlines()

		arr = []

		int0 = int(lines[0][0])
		# start = time.time()
		for line in lines:
			seg = line.split(',')
			if int(seg[0]) == int0:
				## stored as event_no, x, y
				arr.append([int(seg[0]), float(seg[3]), float(seg[4])])
			
			else:
				print(int0)
				##events += numpy.array(test_hits_2d(arr, int(seg[0])))
				
				if int0 == int(event):
					#print(arr)
					plot(arr, int(d0bins), int(phibins))
					return
				
				trueevents += 1
				#y = events/trueevents

				arr = []
				int0 = int(seg[0])
				arr.append([int(seg[0]), float(seg[3]), float(seg[4])])
	
	# bins = []
	# for b in range(1, 100, 10):
	# 	bins.append(b)
	# end = time.time()
	# print(end - start)
	# y = events/trueevents
	# # print(y)
	# numpy.savetxt("output2/final2.txt", y)
	

	# plt.plot(bins, events/trueevents, '-o')
	# plt.xlabel("No. of bins")
	# plt.ylabel("Track Finding Efficiency (10events)")
	# #plt.show()
	# plt.savefig("nov_last/test.png")

def test_hits_2d(arr, event):
	result = []

	file_length = len(arr)

	for xbins in range(140, 150, 1):
		yres = []
		# print(f'	{xbins}')
		for ybins in range(140, 150, 1):
			# x = TCanvas()
			# print(f'		{ybins}')
			h2 = TH2F("h1", "title", xbins, 0, 1.5, ybins, -1000, 1000)
			
			for i in range(0, file_length):
				for phi in range(0, 360):
					phi0 = phi*1.0*numpy.pi/180
					d0 = (float(arr[i][2]) - float(arr[i][1])*numpy.tan(phi0))/numpy.cos(phi0)
					h2.Fill(phi0, d0, 1)

			#h2.Draw("colz")
			#x.Print(f"colz_2d_out/{event}-{xbins}-{ybins}.png")

			hit = 0
			for i in range(1, xbins + 1):
				for j in range(1, ybins + 1):
					gbin = h2.GetBin(i, j)
					w = h2.GetBinContent(gbin)
					if (w > 7):
						hit = 1
						break

			yres.append(hit)
			del h2
				
		result.append(yres)
			
	return result

def plot(arr, xbins, ybins):
	
	c1 = ROOT.TCanvas('c1', 'c1')
	file_length = len(arr)
	
	h2 = TH2F("h1", "Single Muon Collision", xbins, 0, 1.5, ybins, -1000, 1000)				
	for i in range(0, file_length):
		for phi in range(0, 360):
			phi0 = phi*1.0*numpy.pi/180
			
			## d0 = (yhit - xhit * tan(phi0)) / cos(phi0)
			d0 = (float(arr[i][2]) - float(arr[i][1])*numpy.tan(phi0))/numpy.cos(phi0)
			x = numpy.tan(phi0)/numpy.cos(phi0) 
			print(phi, phi0, d0, x)
			h2.Fill(phi0, d0, 1)
	hit = 0
	for i in range(1, xbins + 1):
		for j in range(1, ybins + 1):
			gbin = h2.GetBin(i, j)
			w = h2.GetBinContent(gbin)
			if (w > 7):
				hit = 1

	#h2.SetTitle("false" if hit==0 else "true")
	h2.SetStats(False)
	h2.SetXTitle("phi0")
	h2.SetYTitle("d0")
	h2.Draw("colz")
	c1.Print(f"output/{xbins}-{ybins}-py.png")

	del h2
	del c1


if __name__ == "__main__":
	main()
