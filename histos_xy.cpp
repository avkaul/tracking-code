#include <cmath>
#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <sstream>
#include <chrono>
#include <string>
#include <filesystem>

#include "TH2.h"
#include "TCanvas.h"

using namespace std;
using namespace std::chrono;

int main(){
	int maxevents = 1000;
	int trueevents = 0;
	string target = "ForEFTracking/1kevents/events/";


	for (int i = 0; i < maxevents; i++){
		string x = to_string(i) += ".txt";
		string targ = target += x;
		cout << targ;
	}
/*		namespace fs = std::experimental::filesystem;
		fs::path f{targ};
		
		if (fs::exists(f)){
			trueevents = trueevents + 1;
		}
	}*/

	cout << trueevents;

	return 0;
}

vector<vector<int>> compute(string filename){
	ifstream input(filename);
	int maxbin = 1000;

	int file_length = 0;
	string line, word;

	while (getline(input, line)){
		file_length = file_length + 1;
	}

	string arr[file_length][6];

	int i = 0;
	while (getline(input, line)){
		istringstream iss(line);
		int j = 0;
		while (getline(iss, word, ',')){
			arr[i][j] = word;
			j = j + 1;
		}
		i = i + 1;
	}

	vector<vector<int>> x_vecs;
	for (int xbins = 1; xbins < 1000; xbins = xbins + 20){
		cout << '[';
		vector<int> y_vecs;
		for (int ybins = 100; ybins < 10000; ybins = ybins + 300){
			TH2* h2 = new TH2F("h1", "title", xbins, 0, 1.5, ybins, -1000, 1000) ; // weird bounds?
			for (int i = 1; i < file_length; i++){
				float d0;
				for (int phi = 0; phi < 360; phi++){
					float phi0 = phi*1.0*M_PI/180;
					d0 = (stof(arr[i][4]) - stof(arr[i][3])*tan(phi0))/cos(phi0);
					h2->Fill(phi0, d0, 1);
					}
				}

			int hit = 0;
			for (int i = 1; i < xbins + 1; i++){
				for (int j = 1; j < ybins + 1; j++){
					int gbin = h2->GetBin(i, j);
					int w = h2->GetBinContent(gbin);
					if (w > 7){
						hit = 1;
						break;
					}
				}
			}
			y_vecs.push_back(hit);
			// cout << hit << ",";// << num << endl;
			delete h2;
		}
		x_vecs.push_back(y_vecs);
		y_vecs.clear();
		// cout << "],";
	}
	return x_vecs;
}

		// auto start = high_resolution_clock::now();
// auto stop = high_resolution_clock::now();
	// auto duration = duration_cast<microseconds>(stop - start);
	 
	//cout << "Time taken by function: " << " ";
	// cout << duration.count() << ","; //<< " microseconds" << endl;

	//h2->Draw();
	//c->Print((to_string(bins) + ".png").c_str());
//	}


