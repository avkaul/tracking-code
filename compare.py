import matplotlib.pyplot as plt
import numpy as np

info = {"output_python.txt":[[1, 250, 5], [1, 250, 5]],} #"output.txt":[[1, 1000, 100], [1, 500, 100]], 
j = [[1, 250, 5], [1, 250, 5]]
data = np.genfromtxt("histos/cppout.txt", delimiter=",")[:, :50]
data_python = np.loadtxt("histos/pyout.txt")

fig = plt.figure()
ax = plt.axes(projection='3d')

d0 = [i for i in range(*j[0])]
phi = [i for i in range(*j[1])]
d0, phi = np.meshgrid(phi, d0)

ax.set_xlabel("No. of bins in d0")
ax.set_ylabel("No. of bins in phi0")
ax.set_zlabel("Efficiency")
ax.plot_wireframe(d0, phi, (data-data_python) / 1000)

plt.title("C++ - Python")
plt.show()
#plt.savefig(f'{i[:-4]}.jpg')
