## Tracking Code

Run with either make.sh or provided makefile. Main files:

1. histos.cpp:
Efficiency analysis of events_2d.py implemented in C++.

2. events_2d.py:
Efficiency analysis using pyROOT.
