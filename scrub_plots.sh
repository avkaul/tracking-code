#!/bin/bash

cd output && rm * && cd ..
make

for i in {50..100..50}
do
	for j in {100..250..50}
	do
		python events_2d.py $i $j 1 > pyout.txt
		./a.out $i $j 1 > cppout.txt
		cd output
		convert "${i}-${j}-py.png" "${i}-${j}-cpp.png" -append "${i}-${j}.png"	
		rm "${i}-${j}-py.png" "${i}-${j}-cpp.png"
		cd ..
	done
done
